FROM amazon/aws-cli
WORKDIR /usr/src/helm
ENV VERIFY_CHECKSUM=false
RUN yum update
RUN yum install tar gzip -y
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh
